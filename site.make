core = 7.x
api = 2

; menu_position
projects[menu_position][type] = "module"
projects[menu_position][download][type] = "git"
projects[menu_position][download][url] = "https://git.uwaterloo.ca/drupal-org/menu_position.git"
projects[menu_position][download][tag] = "7.x-1.1"
projects[menu_position][subdir] = ""

; uw_ct_journal_publication
projects[uw_ct_journal_publication][type] = "module"
projects[uw_ct_journal_publication][download][type] = "git"
projects[uw_ct_journal_publication][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_journal_publication.git"
projects[uw_ct_journal_publication][download][tag] = "7.x-1.1.7"
projects[uw_ct_journal_publication][subdir] = ""

